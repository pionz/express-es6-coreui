import bcrypt from 'bcrypt';
import core from '../../../modules/core';
import { BadRequestError } from '../../../../common/errors';

const bookshelf = core.mysql.db;

// used by bcrypt to generate new salt
// 8 rounds will produce about 40 hashes per second on a 2GHz core
// see: https://www.npmjs.com/package/bcrypt
const SALT_ROUND = 8;

class UserModel extends bookshelf.Model {
  // eslint-disable-next-line class-methods-use-this
  get tableName() {
    return 'users';
  }
  // eslint-disable-next-line class-methods-use-this
  get idAttribute() {
    return 'user_id';
  }
  // eslint-disable-next-line class-methods-use-this
  get hasTimestamps() {
    return true;
  }

  serialize() {
    const user = {
      user_id: this.get('user_id'),
      first_name: this.get('first_name'),
      last_name: this.get('last_name'),
      phone: this.get('phone'),
      email: this.get('email'),
      status: this.get('status'),
      username: this.get('username'),
    };
    return user;
  }

  /**
   * Create password hash from plain text
   * @param {string} str
   */
  static async hashPassword(str) {
    return await bcrypt.hash(str, SALT_ROUND);
  }

  /**
   * Create password hash from plain text synchronously
   * @param {string} str
   */
  static hashPasswordSync(str) {
    return bcrypt.hashSync(str, SALT_ROUND);
  }

  static async getAllUser(page, pageSize) {
    return await this.fetchPage({ pageSize, page });
  }

  /**
   * Compare plain password with it's hashed password
   * @param {string} plain
   * @return {bool}
   */
  static async checkPassword(plain, password) {
    return await bcrypt.compareSync(plain, password);
  }

  static async getByEmail(data) {
    return await new this({ email: data }).fetch();
  }

  static async getByIdBasic(data) {
    return await new this({ user_id: data }).fetch();
  }

  static async getByIdProfile(data) {
    const user = await new this({ user_id: data }).fetch({ withRelated: ['group', 'activity', 'following', 'follower'] });
    if (!user) return user;
    return {
      ...user.serialize(),
      group_total: user.related('group').size(),
      activity_total: user.related('activity').size(),
      following_total: user.related('following').size(),
      follower_total: user.related('follower').size(),
    };
  }

  static async create(data) {
    return await new this(data).save().catch((err) => {
      throw new BadRequestError(err, 'Register user failed');
    });
  }

  static async update(data, newData) {
    return await this.where({ user_id: data }).save(newData, { patch: true });
  }

  static async getRequestId(data) {
    const requestId = await this({ user_id: data }).fetch({ columns: 'request_id' });
    return requestId.get('request_id');
  }

  static async getByPhone(data) {
    return await new this({ phone: data }).fetch();
  }
}

export const User = bookshelf.model('User', UserModel);
export default { User };
