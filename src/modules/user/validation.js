import { errMsg } from './error';

const { loginMsg } = errMsg;
const constraints = {};

/**
 * Login
 */
constraints.login = {
  email: {
    presence: { message: loginMsg.email_presence },
    email: { message: loginMsg.email_not_valid },
  },
  password: {
    presence: { message: loginMsg.password_presence },
  },
};

export default constraints;
