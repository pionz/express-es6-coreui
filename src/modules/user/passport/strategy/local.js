import { Strategy as LocalStrategy } from 'passport-local';
import User from '../../model';

const UserModel = User.User;

export default new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: false,
}, async (email, password, done) => {
  const user = await UserModel.getByEmail(email);
  if (!user) return done(null, false);
  const check = await UserModel.checkPassword(password, user.get('password'));
  if (!check) {
    return done(null, false);
  }
  return done(null, user.serialize());
});
