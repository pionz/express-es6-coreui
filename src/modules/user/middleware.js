import _ from 'lodash';
import config from '../../../config';
import utils from '../../../common/utils';

export const ROLE_ALL = '*';

/**
 * Auth middleware
 * @param {array} roles
 * @param {string|function} failedCb
 */
export function auth(roles, failedCb) {
  const reject = (req, res) => {
    if (utils.isFunction(failedCb)) return failedCb(req, res);
    if (!failedCb || failedCb === REDIRECT_LOGIN) return res.redirect(config.loginPath);
    return res.status(401).end();
  };

  return (req, res, next) => {
    if (req.isAuthenticated()) {
      if (!roles || roles === ROLE_ALL) return next();

      roles = utils.isArray(roles) ? roles : [roles];
      const user = req.user || {};
      // fix role
      if (_.includes(roles, user.role)) return next();
    }

    return reject(req, res);
  };
}


/**
 * Make sure only non authenticated user can access the route
 * @param {string|function} failedCb
 */
export function noUser(failedCb) {
  const reject = (req, res) => {
    if (utils.isFunction(failedCb)) return failedCb(req, res);
    if (!failedCb || failedCb === REDIRECT_HOME) return res.redirect('/');
    return res.status(400).end();
  };

  return (req, res, next) => {
    if (req.isAuthenticated()) {
      return reject(req, res);
    }
    return next();
  };
}
