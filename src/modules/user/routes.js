import express from 'express';
import passport from 'passport';
import { UserController } from './controller';
import { auth, noUser } from './middleware';

const routes = express.Router();
const redirect = path => (req, res) => res.redirect(path);

routes.get('/', auth(), (req, res) => res.redirect('/dashboard'));

routes.get('/logout',
  UserController.logout);

routes.get('/login',
  noUser(),
  UserController.loginForm);

routes.post('/login',
  passport.authenticate('local-login', {
    successRedirect: '/dashboard',
    failureRedirect: '/login',
    failureFlash: 'Invalid username or password.',
  }),
  redirect('/login'));

routes.get('/dashboard',
  auth(),
  UserController.dashboard);

export default routes;
